package project_selenium;

import classes.ChessAccounts;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

public class LichessTest {

    private WebDriver driver;
    String url = "http://lichess.org";
    ChessAccounts accounts = new ChessAccounts();

    @Parameters("browser")
    @BeforeClass //аннотация testng
    protected void driverSetup (@Optional("Chrome") String browser) {
        DriverFactory factory = new DriverFactory();
        driver = factory.setup(browser);
        driver.get(url);
    }


    @Test(groups = {"smoke","crm"})
    public  void test_chess() throws InterruptedException {

        for (int i = 0; i < accounts.getAccountsQuantity(); i++) {
            
            WebDriverWait button_to_authorize = new WebDriverWait(driver, 15);
            Thread.sleep(100);
            button_to_authorize.until(ExpectedConditions.visibilityOfElementLocated
                    (By.xpath("//a[contains(@class,'signin button button-empty')]")));
            Thread.sleep(100);
            button_to_authorize.until(ExpectedConditions.elementToBeClickable
                    (By.xpath("//a[contains(@class,'signin button button-empty')]"))).click();
            Thread.sleep(1000);


            WebDriverWait login = new WebDriverWait(driver, 15);
            Thread.sleep(100);
            login.until(ExpectedConditions.visibilityOfElementLocated
                    (By.xpath("//div[contains(@class, 'form-group')]/input[contains(@id, 'form3-username')]")));
            Thread.sleep(100);
            login.until(ExpectedConditions.elementToBeClickable
                    (By.xpath("//div[contains(@class, 'form-group')]/input[contains(@id, 'form3-username')]"))).click();
            Thread.sleep(100);
            login.until(ExpectedConditions.elementToBeClickable
                    (By.xpath("//div[contains(@class, 'form-group')]/input[contains(@id, 'form3-username')]")))
                    .sendKeys(accounts.get_account_by_index(i).first);

            WebDriverWait pass = new WebDriverWait(driver, 15);
            Thread.sleep(100);
            pass.until(ExpectedConditions.visibilityOfElementLocated
                    (By.xpath("//div[contains(@class, 'form-group')]/input[contains(@id, 'form3-password')]")));
            Thread.sleep(100);
            pass.until(ExpectedConditions.elementToBeClickable
                    (By.xpath("//div[contains(@class, 'form-group')]/input[contains(@id, 'form3-password')]"))).click();
            Thread.sleep(100);
            login.until(ExpectedConditions.elementToBeClickable
                    (By.xpath("//div[contains(@class, 'form-group')]/input[contains(@id, 'form3-password')]")))
                    .sendKeys(accounts.get_account_by_index(i).second);

            WebDriverWait button_auth = new WebDriverWait(driver, 15);
            Thread.sleep(100);
            button_auth.until(ExpectedConditions.visibilityOfElementLocated
                    (By.xpath("//div[contains(@class, 'one-factor')]/button[contains(@type, 'submit')]")));
            Thread.sleep(100);
            button_auth.until(ExpectedConditions.elementToBeClickable
                    (By.xpath("//div[contains(@class, 'one-factor')]/button[contains(@type, 'submit')]"))).click();
            Thread.sleep(1000);

            WebDriverWait find_exit = new WebDriverWait(driver, 15);
            Thread.sleep(100);
            find_exit.until(ExpectedConditions.visibilityOfElementLocated
                    (By.xpath("//div[contains(@class,'dasher')]/a[contains(@id, 'user_tag')]")));
            Thread.sleep(100);
            find_exit.until(ExpectedConditions.elementToBeClickable
                    (By.xpath("//div[contains(@class,'dasher')]/a[contains(@id, 'user_tag')]"))).click();
            Thread.sleep(500);

            WebDriverWait exit_button = new WebDriverWait(driver, 15);
            Thread.sleep(100);
            exit_button.until(ExpectedConditions.visibilityOfElementLocated
                    (By.xpath("//form[contains(@class, 'logout')]/button[contains(@type, 'submit')]")));
            Thread.sleep(100);
            exit_button.until(ExpectedConditions.elementToBeClickable
                    (By.xpath("//form[contains(@class, 'logout')]/button[contains(@type, 'submit')]"))).click();
            Thread.sleep(500);
        }

        Thread.sleep(3000);
    }


    @AfterClass
    public void afterClass() {
        driver.quit();
    }
}
