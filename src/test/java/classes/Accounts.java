package classes;



import java.util.*;

public class Accounts {

    HashMap<String,String> Accounts = new HashMap<String,String>();
    String[] keyArray;


    private void put(String nick, String pass){
        Accounts.put(nick, pass);
    }

    public void puts(LinkedList<String> nicks, LinkedList<String> passes){
        if(nicks.size() == passes.size()){
            for (int i = 0; i < nicks.size(); i++) {
                put(nicks.get(i),passes.get(i));
            }
            Set<String> nicknames = Accounts.keySet();

            keyArray = nicknames.toArray( new String[ nicknames.size() ]);
        }
    }

    public Pair<String, String> get_nickname_password_by_index(int Index){

        if ((Index >= 0) && (Index < keyArray.length)) {
            String nickname = keyArray[Index];
            String pass = Accounts.get(nickname);
            Pair<String, String> pair = new Pair<String, String>(nickname,pass);

            return pair;
        }
        return null;
    }

    public Integer get_users_quantity(){
        return Accounts.size();
    }

}
