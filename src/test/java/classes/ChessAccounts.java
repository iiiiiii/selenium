package classes;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;

public class ChessAccounts {

    Accounts accounts = new Accounts();
    Integer accountsQuantity;

    public Integer getAccountsQuantity(){
        return accountsQuantity;
    }

    public ChessAccounts() {
        LinkedList<String> nicknames = new LinkedList<String>
                (Arrays.asList("DaniilKo","testerMarch3","d4__c5","Hercules1111111111","gvalaev"));
        LinkedList<String> passwords = new LinkedList<String>
                (Arrays.asList("463160","88888888","88888888","PerformanceLab","88888888"));
        accounts.puts(nicknames,passwords);
        accountsQuantity = nicknames.size();
    }

    public Pair<String, String> get_account_by_index(int index){
        if ((index >= 0) && (index < accountsQuantity)){
            return accounts.get_nickname_password_by_index(index);
        }
        return null;
    }
}
