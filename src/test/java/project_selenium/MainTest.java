package project_selenium;


import classes.ChessAccounts;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import org.openqa.selenium.remote.server.DriverProvider;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

@Test(groups = {"smoke","crm"})
public class MainTest {

    private WebDriver driver;
    String url = "https://students.bmstu.ru/schedule/list";


    @Parameters("browser")
    @BeforeClass //аннотация testng
    protected void driverSetup (@Optional("Firefox") String browser) {
        DriverFactory factory = new DriverFactory();
        driver = factory.setup(browser);
        driver.get(url);
    }

//    @Test
//    public  void test_frames() throws  InterruptedException{
//        WebDriverWait search01 = new WebDriverWait(driver, 5);
//        search01.until(ExpectedConditions.visibilityOfElementLocated
//                (By.xpath("//a[contains(text(), 'Frames')]")));
//        search01.until(ExpectedConditions.elementToBeClickable
//                (By.xpath("//a[contains(text(), 'Frames')]"))).click();
//        search01.until(ExpectedConditions.visibilityOfElementLocated
//                (By.xpath("//a[contains(text(), 'Nested Frames')]")));
//        search01.until(ExpectedConditions.elementToBeClickable
//                (By.xpath("//a[contains(text(), 'Nested Frames')]"))).click();
//
//        driver.switchTo().frame("frame-bottom");
//        search01.until(ExpectedConditions.visibilityOfElementLocated
//                (By.xpath("//body[contains(text(),'BOTTOM')]")));
//        String text = search01.until(ExpectedConditions.elementToBeClickable
//                (By.xpath("//body[contains(text(),'BOTTOM')]"))).getText();
//
//        System.out.println(text);
//        Thread.sleep(3000);
////        search01.until(ExpectedConditions.visibilityOfElementLocated
////                (By.xpath("//body[contains(text(),'BOTTOM')]")));
////         text = search01.until(ExpectedConditions.elementToBeClickable
////                (By.xpath("//body[contains(text(),'BOTTOM')]"))).getText();
//
//    }




//    @Test
//    public  void test_chess() throws InterruptedException{
//        WebDriverWait search01 = new WebDriverWait(driver, 15);
//        search01.until(ExpectedConditions.visibilityOfElementLocated
//                (By.xpath("//a[contains(@class,'signin button button-empty')]")));
//        search01.until(ExpectedConditions.elementToBeClickable
//                (By.xpath("//a[contains(@class,'signin button button-empty')]"))).click();
//
//
//    }



// click Planes


//    @Test
//    public void my_test() throws InterruptedException {
//        WebDriverWait search01 = new WebDriverWait(driver, 15);
//        search01.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Картинки')]")));
//        WebDriverWait buttonNewPaymentClickable = new WebDriverWait(driver, 15);
//        buttonNewPaymentClickable.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(),'Картинки')]"))).click();
//        buttonNewPaymentClickable.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@type,'search')]"))).sendKeys("Мечта");
//        Thread.sleep(25000);
//        //div[contains(text(), 'Найти')]
//
//    }

    @Test
    public void test_schelude() throws InterruptedException {
        WebDriverWait search01 = new WebDriverWait(driver, 15);
        search01.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@id='ui-id-9']")));
        Thread.sleep(1000);
        search01.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@id='ui-id-9']"))).click();


        search01.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[contains(text(),'ИУ8')]")));
        Thread.sleep(1000);
        search01.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[contains(text(),'ИУ8')]"))).click();
        search01.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'ИУ8-83')]")));
        Thread.sleep(1000);
        search01.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(),'ИУ8-83')]"))).click();
        Thread.sleep(3000);


    }



    @AfterClass
    public void afterClass() {
        driver.quit();
    }
}
